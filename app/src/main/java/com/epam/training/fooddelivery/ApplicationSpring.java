package com.epam.training.fooddelivery;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = "classpath:application.properties")
public class ApplicationSpring {
    public static void main(String[] args) {
        try (ConfigurableApplicationContext applicationContext = new
                AnnotationConfigApplicationContext(ApplicationSpring.class)) {
            Application application = applicationContext.getBean(Application.class);
            application.run();
        }
    }
}
