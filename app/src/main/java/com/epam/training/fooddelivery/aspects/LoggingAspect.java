package com.epam.training.fooddelivery.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LoggingAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

    @Pointcut("@annotation(com.epam.training.fooddelivery.annotations.EnableArgumentLogging)")
    public void enableArgumentLogging() {}

    @Pointcut("@annotation(com.epam.training.fooddelivery.annotations.EnableReturnValueLogging)")
    public void enableReturnValueLogging() {}

    @Pointcut("@annotation(com.epam.training.fooddelivery.annotations.EnableExecutionTimeLogging)")
    public void enableExecutionTimeLogging() {}

    @Before("enableArgumentLogging()")
    public void enableArgumentLoggingAdvice(JoinPoint joinPoint) {

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        LOGGER.info("Method name: [" + signature.getMethod().getName() + "]" +
                ", parameter(s): " + Arrays.toString(joinPoint.getArgs()));
    }

    @Around("enableReturnValueLogging()")
    public Object enableReturnValueLoggingAdvice(ProceedingJoinPoint joinPoint) throws Throwable {

        Object methodResult = joinPoint.proceed();

        LOGGER.info("Method name: [" + joinPoint.getSignature().getName() + "]" +
                ", return value: [" + methodResult.toString() + "]");
        return methodResult;
    }

    @Around("enableExecutionTimeLogging()")
    public Object enableExecutionTimeLoggingAdvice(ProceedingJoinPoint joinPoint) throws Throwable {

        long startAction = System.currentTimeMillis();
        Object methodResult = joinPoint.proceed();
        long endAction = System.currentTimeMillis();

        LOGGER.info("Execution time = " + (endAction - startAction) + " ms");

        return methodResult;
    }
}
