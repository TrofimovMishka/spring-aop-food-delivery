package com.epam.training.fooddelivery.data;

import com.epam.training.fooddelivery.domain.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@PropertySource(value = "classpath:application.properties")
public class FileDataStore implements DataStore {

    private Long orderIdSequence;
    private final String baseDirPath;
    private List<Customer> customers;
    private List<Food> foods;
    private List<Order> orders;
    private static final String FILE_OF_CUSTOMERS = "customers.csv";
    private static final String FILE_OF_FOODS = "foods.csv";
    private static final String FILE_OF_ORDERS = "orders.csv";

    public FileDataStore(@Value("${baseDirPath}") String baseDirPath) {
        this.baseDirPath = baseDirPath;
        init();
    }

    private void init() {
        getFoods();
        getCustomers();
        getOrders();
    }

    @Override
    public List<Customer> getCustomers() {
        if (customers != null) {
            return customers;
        }
        customers = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(Path.of(baseDirPath + FILE_OF_CUSTOMERS))) {
            String line = "";
            while ((line = reader.readLine()) != null) {
                customers.add(createCustomer(line));
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException("Some issues with file "+FILE_OF_CUSTOMERS);
        }
        return customers;
    }

    @Override
    public List<Food> getFoods() {
        if (foods != null) {
            return foods;
        }
        foods = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(Path.of(baseDirPath + FILE_OF_FOODS))) {
            String lineFromFile = "";
            while ((lineFromFile = reader.readLine()) != null) {
                foods.add(createFood(lineFromFile));
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException("Some issues with file "+FILE_OF_FOODS);
        }
        return foods;
    }

    @Override
    public List<Order> getOrders() {
        if (orders != null) {
            return orders;
        }
        orders = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(Path.of(baseDirPath + FILE_OF_ORDERS))) {

            reader.lines()
                    .map(oneLine -> oneLine.split(","))
                    .collect(Collectors.groupingBy(array -> array[0]))
                    .values()
                    .stream()
                    .map(this::createOrderFromLines)
                    .forEach(order -> orders.add(order));

        } catch (IOException | NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            throw new IllegalArgumentException("Some issues with file "+FILE_OF_ORDERS);
        }
        return orders;
    }

    private Order createOrderFromLines(List<String[]> dataFromLines) {
        Order order = new Order();
        String[] firstOrderItem = dataFromLines.get(0);
        Long id = Long.parseLong(firstOrderItem[0]);
        if (orderIdSequence == null || id > orderIdSequence) {
            orderIdSequence = id;
        }
        order.setOrderId(id);
        order.setCustomerId(Long.parseLong(firstOrderItem[1]));
        order.setPrice(BigDecimal.valueOf(Long.parseLong(firstOrderItem[6])));
        LocalDateTime dateTime = parseTime(firstOrderItem[5]);
        order.setTimestampCreated(dateTime);

        List<OrderItem> orderItems = dataFromLines.stream()
                .map(oneLine -> {
                    String foodName = oneLine[2];
                    int amount = Integer.parseInt(oneLine[3]);
                    BigDecimal priceOrderItem = BigDecimal.valueOf(Long.parseLong(oneLine[4]));
                    return getOrderItem(amount, priceOrderItem, foodName);
                })
                .collect(Collectors.toList());
        order.setOrderItems(orderItems);
        addOrderToCustomer(order.getCustomerId(), order);
        return order;
    }

    @Override
    public Order createOrder(Order order) {
        if(orderIdSequence == null ){
            orderIdSequence = 0L;
            order.setOrderId(orderIdSequence);
        }else{
            order.setOrderId(++orderIdSequence);
        }
        if (orders == null) {
            orders = new ArrayList<>();
        }
        orders.add(order);
        return order;
    }

    private LocalDateTime parseTime(String str) {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        return dateTime;
    }

    private OrderItem getOrderItem(int pieces, BigDecimal price, String foodName) {
        OrderItem item = new OrderItem();
        item.setPrice(price);
        item.setPieces(pieces);
        Food food = getFoods()
                .stream()
                .filter(ele -> ele.getName().equals(foodName))
                .findFirst().orElseThrow();
        item.setFood(food);
        return item;
    }

    private Food createFood(String lineFromFile) {
        String[] propertiesOfFood = lineFromFile.split(",");
        if (propertiesOfFood.length < 5) {
            throw new IllegalArgumentException();
        }
        Food food = new Food();
        food.setName(propertiesOfFood[0]);
        food.setCalorie(BigDecimal.valueOf(Long.parseLong(propertiesOfFood[1])));
        food.setDescription(propertiesOfFood[2]);
        food.setPrice(BigDecimal.valueOf(Long.parseLong(propertiesOfFood[3])));
        food.setCategory(getCategory(propertiesOfFood[4]));

        return food;
    }

    private Customer createCustomer(String lineFromFile) {
        String[] propertiesOfCustomer = lineFromFile.split(",");
        if (propertiesOfCustomer.length < 5) {
            throw new IllegalArgumentException();
        }
        Customer customer = new Customer();
        customer.setEmail(propertiesOfCustomer[0]);
        customer.setPassword(propertiesOfCustomer[1]);
        customer.setId(Long.parseLong(propertiesOfCustomer[2]));
        customer.setName(propertiesOfCustomer[3]);
        customer.setBalance(BigDecimal.valueOf(Double.parseDouble(propertiesOfCustomer[4])));

        return customer;
    }

    private Category getCategory(String str) {
        switch (str) {
            case "GRAINS":
                return Category.GRAINS;
            case "FRUIT":
                return Category.FRUIT;
            case "VEGETABLE":
                return Category.VEGETABLE;
            case "DAIRY":
                return Category.DAIRY;
            case "MEAT":
                return Category.MEAT;
            case "SNACK":
                return Category.SNACK;
            case "MEAL":
                return Category.MEAL;
        }
        return null;
    }

    private void addOrderToCustomer(long customerId, Order order) {
        Customer customer = customers.stream()
                .filter(user -> user.getId() == customerId)
                .findFirst()
                .orElseThrow();
        customer.setOrders(List.of(order));
    }
}
