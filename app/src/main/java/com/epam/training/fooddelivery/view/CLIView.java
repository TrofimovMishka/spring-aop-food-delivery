package com.epam.training.fooddelivery.view;

import com.epam.training.fooddelivery.domain.*;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

@Component
public class CLIView implements View {

    @Override
    public User readCredentials() {
        Scanner scanner = new Scanner(System.in);
        User user = new User();

        System.out.print("Enter customer email address: ");
        String email = scanner.next();
        System.out.print("Enter customer password: ");
        String password = scanner.next();

        user.setEmail(email);
        user.setPassword(password);

        return user;
    }

    @Override
    public void printWelcomeMessage(Customer customer) {
        System.out.format("Welcome, %s. Your balance is: %.0f EUR.\n", customer.getName(), customer.getBalance());
    }

    @Override
    public void printAllFoods(List<Food> foods) {
        foods.forEach(food -> System.out.format("- %s %.0f EUR each\n", food.getName(), food.getPrice()));
    }

    @Override
    public Food selectFood(List<Food> foods) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nPlease enter the name of the food you would like to " +
                "buy or delete from the cart: ");
        String foodName = scanner.next();
        Food food = getFood(foods, foodName);
        return food;
    }

    private Food getFood(List<Food> foods, String foodName) {
        Food food = foods
                .stream()
                .filter(ele -> ele.getName().equals(foodName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Invalid input"));
        return food;
    }

    @Override
    public int readPieces() {
        Scanner scanner = new Scanner(System.in);
        int number = -1;
        try {
            System.out.print("How many pieces do you want to buy?" +
                    "This input overwrites the old value in the cart," +
                    "entering zero removes the item completely: ");
            number = scanner.nextInt();
        } catch (InputMismatchException e) {
            throw new IllegalArgumentException();
        }
        return number;
    }

    @Override
    public void printAddedToCard(Food food, int pieces) {
        System.out.format("Added %d piece(s) of %s to the cart\n", pieces, food.getName());
    }

    @Override
    public void printCard(Cart cart) {
        System.out.format("The card has %.0f EUR of foods:\n", cart.getPrice());
        cart.getOrderItems().forEach(item -> System.out.format("%s %d piece(s), %.0f EUR total\n"
                , item.getFood().getName(), item.getPieces(), item.getPrice()));
    }

    @Override
    public void printConfirmOrder(Order order) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        String timeStamp = order.getTimestampCreated().format(formatter);
        System.out.format("\nOrder (items: [%s], price: %.0f EUR, timestamp: %s) has been confirmed." +
                "Thank you for your purchase", getFoods(order), order.getPrice(), timeStamp);
    }

    private String getFoods(Order order) {
        String foods = order.getOrderItems().stream()
                .map(item -> item.getFood().getName())
                .reduce((str1, str2) -> String.join(", ", str1, str2)).orElseThrow();
        return foods;
    }

    @Override
    public boolean promptOrder() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nAre you finished with your order? (y/n) ");
        String answer = scanner.next();

        boolean isContinue = false;

        if (!answer.equals("y") && !answer.equals("n")) {
            System.out.println("Invalid input");
            isContinue = promptOrder();
        } else if (answer.equals("y")) {
            isContinue = true;
        }
        return isContinue;
    }

    @Override
    public void printErrorMessage(String message) {
        System.out.format("You don't have enough money, your balance is only %s EUR." +
                "We do not empty your cart, please remove some of the items\n", message);
    }

    @Override
    public void printMessage() {
        System.out.println("\nThere are our foods today:");
    }

}
