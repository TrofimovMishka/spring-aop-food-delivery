package com.epam.training.fooddelivery.view;

import com.epam.training.fooddelivery.domain.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class CLIViewTest {
    private static List<Food> allFood;
    private View view;

    private PrintStream defaultOutput = System.out;
    private InputStream defaultInput = System.in;

    private OutputStream customOutput = new ByteArrayOutputStream();
    private PrintStream output = new PrintStream(customOutput);

    @BeforeAll
    static void setUp() {
        createListOfFood();
    }

    @BeforeEach
    void setCustomerOutput() {
        System.setOut(output);
        view = new CLIView();
    }

    private static void createListOfFood() {
        Food fideua = new Food();
        fideua.setName("Fideua");
        fideua.setPrice(new BigDecimal("15"));

        Food paella = new Food();
        paella.setName("Paella");
        paella.setPrice(new BigDecimal("13"));

        Food tortilla = new Food();
        tortilla.setName("Tortilla");
        tortilla.setPrice(new BigDecimal("10"));

        allFood = List.of(fideua, paella, tortilla);
    }

    @AfterEach
    void setDefault() {
        System.setOut(defaultOutput);
        System.setIn(defaultInput);
    }

    @Test
    void readCredentialsMethodShouldReturnUser() {
        enterSomeMessage("email\npassword");
        User expectedUser = new User();
        expectedUser.setPassword("password");
        expectedUser.setEmail("email");

        User actualUser = view.readCredentials();

        assertEquals(expectedUser, actualUser);
    }

    @Test
    void showCorrectNameAndBalanceFromCustomer() {
        Customer customer = new Customer();
        customer.setName("Bob");
        customer.setBalance(new BigDecimal("100"));
        String expectedMessage = "Welcome, Bob. Your balance is: 100 EUR.\n";

        view.printWelcomeMessage(customer);
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void showAllFoodsNameAndPrice() {
        String expectedMessage = "- Fideua 15 EUR each\n" +
                "- Paella 13 EUR each\n" +
                "- Tortilla 10 EUR each\n";

        view.printAllFoods(allFood);
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void selectAndReturnFoodFromListOfFood() {
        enterSomeMessage("Tortilla");
        Food expectedFood = new Food();
        expectedFood.setName("Tortilla");
        expectedFood.setPrice(new BigDecimal("10"));

        Food actualFood = view.selectFood(allFood);

        assertEquals(expectedFood.getName(), actualFood.getName());
    }

    @Test
    void failToSelectAndReturnFoodFromListOfFood() throws IllegalArgumentException {
        enterSomeMessage("IncorrectNameOfFood");

        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> {
            view.selectFood(allFood);
        });
        assertNotNull(thrown.getStackTrace());
    }

    @Test
    void selectFoodMethodShouldPrintCorrectMessageToConsole() {
        enterSomeMessage("Tortilla");
        String expectedMessage = "\nPlease enter the name of the food you would like to " +
                "buy or delete from the cart: ";

        view.selectFood(allFood);
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);
    }

    private void enterSomeMessage(String message) {
        ByteArrayInputStream customInput = new ByteArrayInputStream(message.getBytes());
        System.setIn(customInput);
    }

    @Test
    void readPiecesMethodShouldPrintMessageToConsole() {
        enterSomeMessage("1");
        String expectedMessage = "How many pieces do you want to buy?" +
                "This input overwrites the old value in the cart," +
                "entering zero removes the item completely: ";

        view.readPieces();
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void readPiecesMethodShouldReturnNumberOfPieces() {
        enterSomeMessage("1");
        int expectedValue = 1;

        int actualValue = view.readPieces();

        assertEquals(expectedValue, actualValue);
    }

    @Test
    void failToReturnValueIfInputIsIncorrectFromReadPiecesMethod() throws IllegalArgumentException {
        enterSomeMessage("NotANumber");
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> {
            view.readPieces();
        });
        assertNotNull(thrown.getStackTrace());
    }

    @Test
    void printAddedToCardMethodShouldPrintCorrectMessageToConsole() {
        String expectedMessage = "Added 10 piece(s) of Fideua to the cart\n";

        view.printAddedToCard(allFood.get(0), 10);
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);
    }

    private OrderItem createOrderItem() {
        OrderItem orderItemFake = new OrderItem();
        orderItemFake.setFood(allFood.get(0));
        orderItemFake.setPieces(2);
        orderItemFake.setPrice(new BigDecimal("30"));
        return orderItemFake;
    }

    @Test
    void printCardMethodShouldPrintCorrectMessageToConsole() {
        Cart cart = new Cart();
        cart.setPrice(new BigDecimal("30"));
        cart.setOrderItems(List.of(createOrderItem()));
        String expectedMessage = "The card has 30 EUR of foods:\n" +
                "Fideua 2 piece(s), 30 EUR total\n";

        view.printCard(cart);
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void printConfirmOrderMethodShouldPrintCorrectMessageToConsole() {
        Order order = createOrderFake();
        String expectedMessage = "\nOrder (items: [Fideua], price: 30 EUR, timestamp: 20-08-2020 12:12:00)" +
                " has been confirmed.Thank you for your purchase";

        view.printConfirmOrder(order);
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);

    }

    private Order createOrderFake() {
        Order orderFake = new Order();
        orderFake.setOrderId(1L);
        orderFake.setCustomerId(1);
        orderFake.setPrice(new BigDecimal("30"));
        orderFake.setTimestampCreated(LocalDateTime.of(2020, Month.AUGUST, 20, 12, 12));
        orderFake.setOrderItems(List.of(createOrderItem()));

        return orderFake;
    }

    @Test
    void promptOrderMethodShouldReturnTrueIfUserEnter_y() {
        enterSomeMessage("y");
        boolean actualValue = view.promptOrder();

        assertTrue(actualValue);
    }

    @Test
    void promptOrderMethodShouldReturnFalseIfUserEnter_n() {
        enterSomeMessage("n");
        boolean actualValue = view.promptOrder();

        assertFalse(actualValue);
    }

    @Test
    void promptOrderMethodShouldAskUserEnterAnswer() {
        enterSomeMessage("y");
        String expectedMessage = "\nAre you finished with your order? (y/n) ";

        view.promptOrder();
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void promptOrderMethodShouldAskOneMoreTimeIfUserEnterInvalidValue() {
        enterSomeMessage("bla");
        Throwable thrown = assertThrows(NoSuchElementException.class, () -> {
            view.promptOrder();
        });
        assertNotNull(thrown.getStackTrace());
    }

    @Test
    void printErrorMessageMethodShouldPrintCorrectMessageToConsole() {
        String expectedMessage = "You don't have enough money, your balance is only 100 EUR." +
                "We do not empty your cart, please remove some of the items\n";

        view.printErrorMessage("100");
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void printMessageMethodShouldPrintCorrectMessageToConsole() {
        String expectedMessage = "There are our foods today:";

        view.printMessage();
        String actualMessage = customOutput.toString();

        assertEquals(expectedMessage, actualMessage.trim());
    }
}
